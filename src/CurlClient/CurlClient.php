<?php

namespace DH\CurlClient;

use DH\CurlClient\Log\LoggerInterface;

/**
 * Clase que implemente cliente Curl
 */
class CurlClient implements ClientInterface {
  private $logger;
  protected $curlInstance;
  protected $baseUrl;
  protected $urlParams;
  protected $requestType;

  public function __construct(LoggerInterface $logger = null) {
    $this -> baseUrl       = "";
    $this -> urlParams     = array();
    $this -> requestType   = "";
    $this -> logger        = $logger;
    $this -> curlInstance  = empty($this -> baseUrl) ? curl_init() : curl_init($baseUrl);
  }

  /**
   * @param string $baseUrl
   * return ClientInterface
   */
  public function setBaseUrl($baseUrl) {
    $this -> baseUrl = $baseUrl;
    curl_setopt($this -> curlInstance, CURLOPT_URL, $this -> baseUrl);
    $this -> logger -> info('URL seteada');
  }

  /**
   * @param string $string
   * return ClientInterface
   */
  public function addToUrl($string) {
    array_push($this -> urlParams, $string);
    $this -> logger -> info('Anhadido parametro a URL');
  }

  /*
   * @param string $user
   * @param string $passwd
   * return ClientInterface
   */
  public function useAuthentication($user, $passwd) {
    if (curl_setopt($this -> curlInstance, CURLOPT_USERPWD, $user . ":" . $passwd)) {
      $this -> logger -> info('Usuario autenticado');
      return true;
    }
    $this -> logger -> error('Error en autenticacion');
    return false;
  }

  /**
   * @param string $string
   * return ClientInterface
   */
  public function getRequestType() {
    return $this -> requestType;
    $this -> logger -> info('requestType: ' . $this -> requestType);
  }

  /**
   * @param string $string
   * return ClientInterface
   */
  public function setRequestType($requestType) {
    if ($this -> requestType != $requestType) {
      if (curl_setopt($this -> curlInstance, CURLOPT_CUSTOMREQUEST, $requestType)) {
        $this -> requestType = $requestType;
        $this -> logger -> info('Seteado requestType: ' . $requestType);
        return true;
      }
      $this -> logger -> error('Error seteando requestType');
      return false;
    }
    $this -> logger -> info('Seteado requestType: ' . $requestType);
    return true;
  }

  /**
   * @param string $name
   * @param string $value
   * return ClientInterface
   */
  public function addParam($name, $value) {
    if (curl_setopt($this -> curlInstance, CURLOPT_POSTFIELDS, array($name => $value))) {
      $this -> logger -> info('Anhadido parametro POST: ' . $name . '-' . $value);
      return true;
    }
    $this -> logger -> error('Error anhadiendo parametro POST: ' . $name . '-' . $value);
    return false;
  }

  /**
   * return mixed
   */
  public function call() {
    if (!is_resource($this -> curlInstance)) {
      $this -> curlInstance = empty($baseUrl) ? curl_init() : curl_init($baseUrl);

      if (!is_resource($this -> curlInstance)) {
        $this -> curlInstance = null;
        $this -> logger -> error('Error creando instancia CURL');
        return false;
      }
    }
    $this -> logger -> info('Instancia creada');
    curl_setopt($this -> curlInstance, CURLOPT_SSL_VERIFYPEER, false);
    return curl_exec($this -> curlInstance);
  }

  /**
   * return ClientInterface
   */
  public function reset() {
    curl_reset($this -> curlInstance);
    $this -> logger -> info('Reset realizado');

  }

}
