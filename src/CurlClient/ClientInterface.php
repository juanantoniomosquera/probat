<?php

namespace DH\CurlClient;

interface ClientInterface {

  const REQUEST_TYPE_POST = 'POST';
  const REQUEST_TYPE_GET = 'GET';
  const REQUEST_TYPE_PUT = 'PUT';
  const REQUEST_TYPE_DELETE = 'DELETE';
  const REQUEST_TYPE_PATCH = 'PATCH';

  /**
   * @param string $baseUrl
   * return ClientInterface
   */
  public function setBaseUrl($baseUrl);

  /**
   * @param string $string
   * return ClientInterface
   */
  public function addToUrl($string);

  /**
   * @param string $user
   * @param string $passwd
   * return ClientInterface
   */
  public function useAuthentication($user, $passwd);

  /**
   * @param string $string
   * return ClientInterface
   */
  public function setRequestType($requestType);

  /**
   * @param string $name
   * @param string $value
   * return ClientInterface
   */
  public function addParam($name, $value);

  /**
   * return mixed
   */
  public function call();

  /**
   * return ClientInterface
   */
  public function reset();
}
